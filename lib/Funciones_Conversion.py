from sympy.abc import x
from sympy import ZZ, Poly
import numpy as np
from lib.Funciones_char_bytes import *


def array_to_Poly(array):
    long = len(array) - 1
    pot = long
    f = 0
    for i in array:
        if pot == 0:
            f = f + i
        else:
            f = f + i * x ** pot
        pot = pot - 1
    return f


def Poly_to_array(poly):
    poly = Poly(poly)
    return poly.all_coeffs()


def String_to_array(string):
    array = [-1]
    for char in string:
        array = np.append(array, char_to_byte(char))
        array = np.append(array, -1)
    return array


def array_to_String(array):
    array2 = []
    string = ""
    primero = 1
    for i in array:
        if i != -1 and primero == 0:
            array2 = np.append(array2, i)
        elif i == -1 or len(array2) == 7:
            if primero == 0:
                #print(array2[::-1])
                string += byte_to_char(array2[::-1])
                array2 = []
            else:
                primero = 0
        if(len(array2) > 7):
            break
    return string

def array_to_list(m):
    m = list(m)
    for i in range(len(m)):
        m[i] = int(m[i])
        
    return m