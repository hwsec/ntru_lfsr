# include <stdlib.h>
# include <math.h>

void ntru_mult (int N, int q, int h[], int r[], int m[], int e[]) {
    for (int a = 0; a <= N-1; a++){
        e[a] = m[a];
    }

    for (int j = 0; j <= N-1; j++){
        if (r[j] != 0 && r[j] == 1){
            for (int i = 0; i <= N-1; i++){
                int k = (i + j) % N;
                e[k] = e[k] + h[i];
            }
        }
        else if (r[j] != 0 && r[j] == -1){
                for (int i = 0; i <= N-1; i++){
                int k = (i + j) % N;
                e[k] = e[k] - h[i];
            }
        }
    }

// Truncamiento de la operacion
    for (int i = 0; i <= N-1; i++){
        e[i] = e[i] % q;
        if (e[i] >= (q/2)){
            e[i] -= q;
        }
        else if(e[i] < -(q/2)){
            e[i] += q;
        }
        
        if(e[i] == -2 && q == 3){
            e[i] = 1;
        }
        
    }
}