import random
import math
from sympy import GF, invert
import numpy as np
from sympy.abc import x
from sympy import ZZ, Poly

np.set_printoptions(suppress=True)

'''Multiplicacion NTRU'''
def ntru_mult_python(N, q, h, r, m):
    #Inicializacion de parametros
    h = h[::-1]
    r = r[::-1]
    
    e = np.zeros(N)
    for i in range(len(m)):
        e[i] = m[i]
    
    #Multiplicacion
    for j in range(len(r)):
        if (r[j] != 0 and r[j] == 1):
            for i in range(len(h)):
                pos = (i + j) % N
                e[pos] = (e[pos] + h[i])
        elif (r[j] != 0 and r[j] == -1):
            for i in range(len(h)):
                pos = (i + j) % N
                e[pos] = (e[pos] - h[i])

    #Modulo q

    for i in range(N):
        e[i] = e[i] % q
        if (e[i] >= q / 2):
            e[i] = e[i] - q

    e = e[::-1]
    return e

''' Para obtener un polinomio aleatoriamente'''
def array_aleatorio(N,dr, diff):

    r =[0]*N
    n1 = 0
    while n1 < dr:
        x = random.randrange(N)
        if r[x] == 0:
            r[x] = 1
            n1 = n1 + 1
        
    n1 = 0
    while n1 < dr+diff:
        x = random.randrange(N)
        if r[x] == 0:
            r[x] = -1
            n1 = n1 + 1
        
    return r

def truncar_polinomio(e,q):
    for i in range(len(e)):
        e[i] = e[i] % q
        if (e[i] >= q / 2):
            e[i] = e[i] - q
            
    return e
    

def is_prime(n):
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            return False
    return True


def is_2_power(n):
    return n != 0 and (n & (n - 1) == 0)


def random_poly(length, d, neg_ones_diff=0):
    return Poly(np.random.permutation(np.concatenate((np.zeros(abs(length - 2 * d - neg_ones_diff)), np.ones(abs(d)), -np.ones(abs(d + neg_ones_diff))))),x).set_domain(ZZ)


def invert_poly(f_poly, R_poly, p):
    inv_poly = None
    if is_prime(p):
        inv_poly = invert(f_poly, R_poly, domain=GF(p))
    elif is_2_power(p):
        inv_poly = invert(f_poly, R_poly, domain=GF(2))
        e = int(math.log(p, 2))
        for i in range(1, e):
            inv_poly = ((2 * inv_poly - f_poly * inv_poly ** 2) % R_poly).trunc(p)
    else:
        return True
    return inv_poly