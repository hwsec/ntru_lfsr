# NTRU_LFSR PROJECT

This project contains the tests used to analyze the performance of hardware implementations of polynomial multipliers for NTRU-based cryptographic schemes proposed in the paper:

<b><i>Timing-Optimized Hardware Implementation to Accelerate the Polynomial Multiplication in NTRU Algorithm</i></b>
<br>
by <b>E. Camacho-Ruíz, S. Sánchez-Solano, P. Brox , and M. C. Martínez-Rodríguez.</b>
<br>
Submitted for publication to <b>ACM Journal on Emerging Technologies in Computing Systems</b>
 

## Execution environment

Tests were designed and executed on a Tul Pynq-Z2 development board (powered by a Xilinx's Zynq-7000 Soc device) using the PYNQ (Python Productivity for Zynq) environment.
<br><br>
The project's directory structure provides the material required to reproduce the results included in the paper. Basically, it includes:
<ul> 
<li>Bitstreams files for hardware implementation of three multipliers of different degrees</li>
<li>Python and C functions used to validate and compare different implementation strategies</li>
<li>A Jupyter Notebook to easy  test execution and results visualization</li>
</ul>

## Related links

<ul> 
<li><a href="https://www.tul.com.tw/productspynq-z2.html">TUL Pynq-Z2 Board</li>
<li><a href="http://www.pynq.io/">PYNQ: Python Productivity for zynq</li>
<li><a href="https://jupyter.org/">Jupyter Notebook</li>
</ul>