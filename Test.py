#############################################################
##
##         POLYNOMIAL MULTIPLICATION TESTING NTRU
## ----------------------------------------------------------
## 
## Eros Camacho Ruiz
## camacho@imse-cnm.csic.es
## 01/05/2020
##
## This file provide a complete testing for NTRU polynomial 
## multiplicacion. It has been tested three different cases: 
##        1. Only Python Software
##        2. Only Python + C Software
##        3. Python + C HW/SW Codesign
############################################################


## Initialization of needeed libraries
from lib.Funciones_Matematicas import *
from lib.Funciones_Overlay import *
from lib.Funciones_Conversion import *
from time import time
from time import sleep
from lib.lib_ntru_mult import lib, ffi
#from lib.lib_ntru_mult.lib import ntru_mult
from pynq import Overlay
from pynq import Xlnk



## Definition of Test function with two inputs:
##       1. N -> The polynomial degree
##       2. iterations -> The number of iterations to calculate the time average
## ---------------------------------------------------------------------------
def Test(N,iterations):
    
    print(" \t \t ---------- NTRU POLINOMIAL MULTIPLICATION TESTING -------- \n ") 
    print("Initializing test ", end = "") 
    
    # This function it is used to obtain parameter dr.
    [f,f_p,h,m,dr] = definir(N)
    print(" . ", end = "") 

    # With this function we generate a random ternary polynomial for the blinding
    # polinomial and the message
    r = array_aleatorio(N,dr,0)
    m = array_aleatorio(N,dr,0)
    print(" . ", end = "") 

    # Associate the same index to Python
    r_c = r[::-1]
    h_c = h[::-1]
    r_hw = r[::-1]
    h_hw = h[::-1]
    print(" . ", end = "") 

    # Initialize the variable related to time
    time_total_python = 0
    time_total_c = 0
    time_total_hardware = 0
    print(" . ", end = "") 

    # Initialize other parameters
    q = 2048
    p = 3
    it = 1
    print(" . Done ") 
    
    ## ----------------------------------------------- ##
    ##         INITIALIZING OF THE HARDWARE            ##
    ## ----------------------------------------------- ##
    print("Initializing Hardware ", end = "") 
    string = 'LFSR/'+str(N)+'/Montaje_v2_wrapper.bit'
    overlay = Overlay(string)
    print(" . ", end = "") 

    dma_rw = overlay.axi_dma_0
    dma_r = overlay.axi_dma_1
    xlnk = Xlnk()
    print(" . ", end = "") 

    input_buffer_1 = xlnk.cma_array(shape=(N,), dtype = np.int32)
    input_buffer_2 = xlnk.cma_array(shape=(N,), dtype = np.int32)
    output_buffer = xlnk.cma_array(shape=(N,), dtype = np.int32)
    print(" . Done ") 
    # ------------------------------------------------ ##
    
    # Generate the buffer needed to obtain C results.
    string_buffer = "int["+str(N)+"]"
    buffer = ffi.new(string_buffer)

    print("Starting iterations . . ") 
    while it <= iterations:
        print(" \t \t ---------- ITERATION ", it, " ---------- ")
        #------------------------#
        # PYTHON MULTIPLICATION
        time_python = time()
        e_python = ntru_mult_python(N, q, h, r, m)
        time_python = time()-time_python
        # -----------------------#
        
        #------------------------#
        # C MULTIPLICATION
        time_c = time()
        lib.ntru_mult(N,q,h_c,r_c,m,buffer)
        e_c = ffi.unpack(buffer, N)
        time_c = time()-time_c
        #------------------------#
        
        #------------------------#
        #HARDWARE MULTIPLICATION
        # Function to join blind polynomial and the message
        rm = obtener_rm(N,r_hw,m)

        time_hardware = time()
        # Copy inputs in buffer
        input_buffer_1[:] = h_hw[:]
        input_buffer_2[:] = rm[:]

        # Encrypt with the IP module
        dma_rw.sendchannel.transfer(input_buffer_1)
        dma_r.sendchannel.transfer(input_buffer_2)
        dma_rw.recvchannel.transfer(output_buffer)

        # Copy the results in a variable
        e_hw = output_buffer.copy()

        time_hardware = time()-time_hardware 
        #-----------------------#
        
        time_total_python += time_python
        time_total_c += time_c
        time_total_hardware += time_hardware
        
        # Associate the index to Python 
        e_c = e_c[::-1]
        e_hw = e_hw[::-1]
        
        # End of this iteration
        
        sleep(1)

        print('Python: ', time_python*1000, ' \t \t C: ', time_c*1000, '\t \t HW: ', time_hardware*1000)
        
        #Check it is obtained the same results for each multiplication
        if((e_python == e_hw).all() and (e_python == e_c).all()):
            print('Done ... The results of the tests are similar ... \n')
        
        # Make a reset to initialize with the same 
        overlay = Overlay(string)
        dma_rw = overlay.axi_dma_0
        dma_r = overlay.axi_dma_1
        
        it = it + 1
    
    it = it - 1
    print(" \t \t ---------- RESULTS OF THE TEST (Average time) ---------- ") 
    print('Python: ', time_total_python*1000/it, ' \t \t C: ', time_total_c*1000/it , '\t \t HW: ', time_total_hardware*1000/it)